var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());

let danhSach = [
   {
      id: 1,
      name: 'Doremon and nobita',
      type: 'hoat hinh'
   }
];
// lay data 
app.get('/books', function (req, res) {
   res.send(danhSach);
})

app.get('/book/:id', function (req, res) {
   let bookId = req.params.id;

   let book = danhSach.find(book => book.id == bookId);
   res.send(book);
})
 
app.delete('/book/:id', function (req, res) {
   let bookId = req.params.id;
   let index = danhSach.findIndex(book => book.id == bookId);
   if (index > -1) { 
      danhSach.splice(index, 1);
      res.send("Xoa thanh cong");
   } else {
      res.send("khong tin thay sach")
   }
   res.send(book);
})


// lay data tu nguoi dung
app.post('/book', function (req, res) {
   let newBook = {
      id: req.body.id,
      name: req.body.name,
      type: req.body.type
   }
   danhSach = [...danhSach, newBook];
   res.send(danhSach);
});

app.put('/book/:id', function (req, res) {
   let bookId = req.params.id;
   // tim sach can chinh sua, update sach vua tim duoc
   let index = danhSach.findIndex(book => book.id == bookId);
   
   if (index > -1) { // index > -1 => tim thay book trong array danhSach
      // update sach vi tri index
      danhSach[index] = {
         id: bookId,
         name: req.body.name,
         type: req.body.type 
      };
      res.send("update thanh cong");
   } else {
      // khong tim thay book trong array danhSach
      res.send("update that bai khong tim thay id");
   }
  
});

var server = app.listen(8080, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://127.0.0.1:8080", host, port)
})



